using Dalamud;
using Dalamud.Interface;
using Dalamud.Interface.ManagedFontAtlas;
using Dalamud.Interface.Utility;
using ImGuiNET;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace LMeter.Helpers
{
    public struct FontData
    {
        public string Name;
        public int Size;
        public bool Chinese;
        public bool Korean;

        public FontData(string name, int size, bool chinese, bool korean)
        {
            Name = name;
            Size = size;
            Chinese = chinese;
            Korean = korean;
        }
    }

    public class FontScope : IDisposable
    {
        private readonly IFontHandle? _handle;

        public FontScope(IFontHandle? handle)
        {
            _handle = handle;
            _handle?.Push();
        }

        public void Dispose()
        {
            _handle?.Pop();
            GC.SuppressFinalize(this);
        }
    }

    public class FontsManager : IDisposable
    {
        private IEnumerable<FontData> _fontData;
        private readonly Dictionary<string, IFontHandle> _imGuiFonts = new ();
        private string[] _imGuiFontsKeysAsArrayCache = Array.Empty<string>();
        public readonly UiBuilder UiBuilder;
        public IFontHandle CactbotAlertFontHandle;
        public static readonly List<string> DefaultFontKeys =
            new ()
            {
                "Expressway_24",
                "Expressway_20",
                "Expressway_16"
            };

        public static string DefaultBigFontKey =>
            DefaultFontKeys[0];
        public static string DefaultMediumFontKey =>
            DefaultFontKeys[1];
        public static string DefaultSmallFontKey =>
            DefaultFontKeys[2];

        public FontsManager(UiBuilder uiBuilder, IEnumerable<FontData> fonts)
        {
            _fontData = fonts;
            UiBuilder = uiBuilder;
            this.BuildFonts();

            this.CactbotAlertFontHandle = this.UiBuilder.FontAtlas.NewDelegateFontHandle
            (
                e => e.OnPreBuild
                (
                    tk =>
                    {
                        var config = new SafeFontConfig
                        {
                            SizePx = 24 * ImGuiHelpers.GlobalScale,
                            GlyphRanges = this.GetCharacterRanges(ImGui.GetIO(), true, true),
                        };
                        tk.Font = tk.AddDalamudAssetFont(DalamudAsset.NotoSansJpMedium, config);
                    }

                )
            );
        }

        public void BuildFonts()
        {
            var fontDir = GetUserFontPath();
            if (string.IsNullOrEmpty(fontDir)) return;

            _imGuiFonts.Clear();
            var io = ImGui.GetIO();

            foreach (var font in _fontData)
            {
                var fontPath = $"{fontDir}{font.Name}.ttf";
                if (!File.Exists(fontPath)) continue;

                try
                {
                    var imFont = this.UiBuilder.FontAtlas.NewDelegateFontHandle
                    (
                        e => e.OnPreBuild
                        (
                            tk => tk.AddFontFromFile
                            (
                                fontPath,
                                new SafeFontConfig
                                {
                                    SizePx = font.Size,
                                    GlyphRanges = this.GetCharacterRanges(io, font.Chinese, font.Korean),
                                }
                            )
                        )
                    );

                    _imGuiFonts.Add(GetFontKey(font), imFont);
                }
                catch (Exception ex)
                {
                    LMeterLogger.Logger?.Error($"Failed to load font from path [{fontPath}]!");
                    LMeterLogger.Logger?.Error(ex.ToString());
                }
            }

            _imGuiFontsKeysAsArrayCache = _imGuiFonts.Keys.ToArray();
        }

        public static bool ValidateFont(string[] fontOptions, int fontId, string fontKey) =>
            fontId < fontOptions.Length && fontOptions[fontId].Equals(fontKey);

        public FontScope PushFont(string fontKey)
        {
            if (!string.IsNullOrEmpty(fontKey))
            {
                if (_imGuiFonts.TryGetValue(fontKey, out var handle))
                {
                    return new FontScope(handle);
                }
            }

            return new FontScope(null);
        }

        public FontScope PushFont(IFontHandle fontPtr)
        {
            return new FontScope(fontPtr);
        }

        public void UpdateFonts(IEnumerable<FontData> fonts)
        {
            foreach (var value in this._imGuiFonts.Values)
            {
                value.Dispose();
            }
            _imGuiFonts.Clear();

            _fontData = fonts;
            this.BuildFonts();
        }

        public string[] GetFontList() =>
            this._imGuiFonts.Keys.ToArray();

        public int GetFontIndex(string fontKey)
        {
            for (var i = 0; i < _imGuiFontsKeysAsArrayCache.Length; i++)
            {
                if (_imGuiFontsKeysAsArrayCache[i].Equals(fontKey))
                {
                    return i;
                }
            }

            return 0;
        }

        private unsafe ushort[]? GetCharacterRanges(ImGuiIOPtr io, bool chinese, bool korean)
        {
            if (!chinese && !korean) return null;

            using (ImGuiHelpers.NewFontGlyphRangeBuilderPtrScoped(out var builder))
            {
                if (chinese)
                {
                    // GetGlyphRangesChineseFull() includes Default + Hiragana, Katakana, Half-Width, Selection of 1946 Ideographs
                    // https://skia.googlesource.com/external/github.com/ocornut/imgui/+/v1.53/extra_fonts/README.txt
                    builder.AddRanges(io.Fonts.GetGlyphRangesChineseFull());
                }

                if (korean)
                {
                    builder.AddRanges(io.Fonts.GetGlyphRangesKorean());
                }

                return builder.BuildRangesToArray();
            }
        }

        public static string GetFontKey(FontData font) =>
            $"{font.Name}_{font.Size}" +
            (
                font.Chinese
                    ? "_cnjp"
                    : string.Empty
            ) +
            (
                font.Korean
                    ? "_kr"
                    : string.Empty
            );

        public static void CopyPluginFontsToUserPath()
        {
            var pluginFontPath = GetPluginFontPath();
            var userFontPath = GetUserFontPath();

            if (string.IsNullOrEmpty(pluginFontPath) || string.IsNullOrEmpty(userFontPath)) return;

            try
            {
                Directory.CreateDirectory(userFontPath);
            }
            catch (Exception ex)
            {
                LMeterLogger.Logger?.Warning($"Failed to create User Font Directory {ex}");
            }

            if (!Directory.Exists(userFontPath)) return;

            string[] pluginFonts;
            try
            {
                pluginFonts = Directory.GetFiles(pluginFontPath, "*.ttf");
            }
            catch
            {
                pluginFonts = Array.Empty<string>();
            }

            foreach (var font in pluginFonts)
            {
                try
                {
                    if (!string.IsNullOrEmpty(font))
                    {
                        var fileName = font.Replace(pluginFontPath, string.Empty);
                        var copyPath = Path.Combine(userFontPath, fileName);
                        if (!File.Exists(copyPath))
                        {
                            File.Copy(font, copyPath, false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LMeterLogger.Logger?.Warning($"Failed to copy font {font} to User Font Directory: {ex}");
                }
            }
        }

        public static string GetPluginFontPath() =>
            Path.Join(MagicValues.ThisAssemblyDirectoryLocation, "Media\\Fonts\\");

        public static string GetUserFontPath() =>
            Path.Join(Plugin.ConfigFileDir, "Fonts\\");

        public static string[] GetFontNamesFromPath(string? path)
        {
            if (string.IsNullOrEmpty(path)) return Array.Empty<string>();

            string[] fonts;
            try
            {
                fonts = Directory.GetFiles(path, "*.ttf");
            }
            catch
            {
                fonts = Array.Empty<string>();
            }

            for (var i = 0; i < fonts.Length; i++)
            {
                fonts[i] = fonts[i]
                    .Replace(path, string.Empty)
                    .Replace(".ttf", string.Empty, StringComparison.OrdinalIgnoreCase);
            }

            return fonts;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _imGuiFonts.Clear();
            }
        }
    }
}
